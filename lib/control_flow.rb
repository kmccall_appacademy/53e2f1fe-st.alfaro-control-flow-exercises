# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.select{ |element| element if element == element.upcase }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  count = str.length
  mid_point = count / 2
  if count.odd?
    str[mid_point]
  else
    str[mid_point - 1, 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count(VOWELS.join)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return num if num == 1
  return num * factorial(num - 1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  join_val = String.new
  arr.each_with_index do |value, index|
    join_val << separator unless index == 0
    join_val << value
  end
  join_val

end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcase_val = String.new
  str.chars.each_with_index do |chr, index|
    if index.even?
      weirdcase_val << chr.downcase
    else
      weirdcase_val << chr.upcase
    end
  end
  weirdcase_val
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  split_word = str.split(" ")
  split_word.map! do |word|
    if word.length < 5
      word
    else
      word.chars.reverse.join
    end
  end
  split_word.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a
  arr.map do |element|
    new_element = String.new
    new_element << "fizz" if element % 3 == 0
    new_element << "buzz" if element % 5 == 0
    new_element.empty? ? element : new_element
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each do |element|
    return false if num % element == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each { |element| factors << element if num % element == 0 }
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |element| prime?(element)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each do |element|
    even_element = element.even?
    if even_element
      return element if odd.count > 1
      even << element
    else
      return element if even.count > 1
      odd << element
    end
  end
  odd.count == 1 ? odd.first : even.first
end
